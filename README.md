import numpy as np
import math as m

roll = 0
pitch = 0

pz = 38.5

A = m.radians(roll)
B = m.radians(pitch)

#Rotation matrix
Rot = ([np.cos(A), 0, -np.sin(B)],[-np.sin(A)*np.sin(B) ,np.cos(A), -np.sin(A)*np.cos(B)],[np.cos(A)*np.sin(B), np.sin(A), np.cos(A)*np.cos(B)])
p = ([0, 0, pz])


Pb = np.dot(Rot,p) #multiplication of matrix Rot and p


b1 = [45.963,0,0]
b2 = [-22.98,39.8,0]
b3 = [-22.98,-39.8,0]

l1 =m.sqrt((Pb[0]-b1[0])**2+(Pb[1]-b1[1])**2+(Pb[2]-b1[2])**2))
l2=m.sqrt((Pb[0]-b2[0])**2   +  (Pb[1]-b2[1])**2  +(Pb[2]-b2[2])**2)
l3=m.sqrt((Pb[0]-b3[0])**2   +  (Pb[1]-b3[1])**2  +(Pb[2]-b3[2])**2)

print("L1 = ",l1)
print("L2 = ",l2)
print("L3 = ",l3)